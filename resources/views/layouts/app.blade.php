<!DOCTYPE html>
<?php
 $side = Request::segment(1) ==''? Request::segment(1): Request::segment(1);
 $subside = Request::segment(2)==''? Request::segment(1): Request::segment(2);
?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('font-awesome/4.5.0/css/font-awesome.min.css') }}" type="text/css" />

		<link rel="stylesheet" href="{{ asset('css/fonts.googleapis.com.css') }}" type="text/css" />


		<link rel="stylesheet" href="{{ asset('css/ace.min.css') }}" type="text/css" />

		<link rel="stylesheet" href="{{ asset('css/ace-skins.min.css') }}" type="text/css" />

		<link rel="stylesheet" href="{{ asset('css/ace-rtl.min.css') }}" type="text/css" />


		<?php if(empty($setting['analytics'])){ echo '';} else {echo $setting['analytics'];}  ?>

		<?php
			if(!empty($css)):
				foreach ($css as $css):
		?>
		<link rel="stylesheet" href="{{ asset('css/'.$css.'.css');?>" type="text/css" />
		<?php
				endforeach;
			endif;
		?>

		<script src="{{ asset('js/ace-extra.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
		<script src="{{ asset('js/ace-elements.min.js') }}"></script>
		<script src="{{ asset('js/ace.min.js') }}"></script>
		<!-- <?php
			if(!empty($js)):
				foreach($js as $js):
		?>
		<script type="text/javascript" src="{{ asset('js/'.$js.'.js')?>"></script>
		<?php
				endforeach;
			endif;
		?> -->
	<link href="https://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />


</head>
<body class="no-skin">
  <div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
      <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
        <span class="sr-only">Toggle sidebar</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>
      </button>

      <div class="navbar-header pull-left">
        <a href="{{ route('home') }}" class="navbar-brand">
          <small>
            <i class="fa fa-leaf"></i>
            Kevin Bank System
          </small>
        </a>
      </div>

      <div class="navbar-buttons navbar-header pull-right" role="navigation">
        <ul class="nav ace-nav">


          <li class="light-blue dropdown-modal">
            @if(!empty(Auth::user()))
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
            <i class="ace-icon fa fa-power-off"></i>
             {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @endif
            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
              <!--
              <li>
                <a href="#">
                  <i class="ace-icon fa fa-cog"></i>
                  Settings
                </a>
              </li>

              <li>
                <a href="profile.html">
                  <i class="ace-icon fa fa-user"></i>
                  Profile
                </a>
              </li>
              -->

              <li class="divider"></li>

              <li>

              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div><!-- /.navbar-container -->
  </div>
  <div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
      try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    @if(!empty(Auth::user()))
    <div id="sidebar" class="sidebar responsive ace-save-state"><script type="text/javascript">
      try{ace.settings.loadState('sidebar')}catch(e){}
    </script>


    <ul class="nav nav-list">
      <li <?php if($side == 'dashboard'){ echo "class='active'"; }?>>
        <a href="{{route('home')}}">
          <i class="menu-icon fa fa-tachometer"></i>
          <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
      </li>

      <li <?php if(!empty($subside)){ echo "class='active open'"; }?>>
        <a href="{{url('account')}}" class="dropdown-toggle">
          <i class="menu-icon fa fa-desktop"></i>
          <span class="menu-text">
            User Menu
          </span>

          <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
          @if( Auth::user()->level == 'client')
          <li <?php if($side == 'account'){ echo "class='active'"; }?>>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-caret-right"></i>
              User Transaction
              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
              <li class="">
                <a href="{{url('account')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Account
                </a>

                <b class="arrow"></b>
              </li>

              <li class="">
                <a href="{{url('account/deposit')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Deposit
                </a>

                <b class="arrow"></b>
              </li>

              <li class="">
                <a href="{{url('account/withdraw')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Withdraw
                </a>

                <b class="arrow"></b>
              </li>

              <li class="">
                <a href="{{url('account/transfer')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Transfer Balance
                </a>

                <b class="arrow"></b>
              </li>

              <li class="">
                <a href="{{url('account/mutasi')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Mutasi Rekening
                </a>

                <b class="arrow"></b>
              </li>

            </ul>
          </li>
          @endif
          @if( Auth::user()->level == 'admin')
          <li <?php if($subside == 'whatsnew'){ echo "class='active'"; }?>>
            <a href="#" class="dropdown-toggle">
              <i class="menu-icon fa fa-caret-right"></i>
                Report
              <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
              <li class="">
                <a href="{{url('account/moneyTotal')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Money's total
                </a>

                <b class="arrow"></b>
              </li>

              <li class="">
                <a href="{{url('account/allUser')}}">
                  <i class="menu-icon fa fa-caret-right"></i>
                  All User and Account
                </a>

                <b class="arrow"></b>
              </li>
            </ul>
          </li>
          @endif

          </ul>
        </li>
      </ul><!-- /.nav-list -->
      <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
      </div>
    </div>
    @endif

            @yield('content')

    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <div class="footer">
          <div class="footer-inner">
            <!-- <div class="footer-content">
              <span class="bigger-120">
                <span class="Green bolder">Kevin Bank</span>
                System &copy; <?php echo date('Y') ?>
              </span>

              &nbsp; &nbsp;
              <span class="action-buttons">
                <a href="#">
                  <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                </a>

                <a href="#">
                  <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                </a>

                <a href="#">
                  <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                </a>
              </span>
            </div> -->
          </div>
        </div>
  </div>
</body>
</html>
