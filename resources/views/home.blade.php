@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="float:left;">
                <div class="card-header">Your Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! <br/>

                    Hello {{ Auth::user()->name }} <br/>
                    Email Anda : {{ Auth::user()->email }} <br/>
                    Anda login menggunakan username : {{ Auth:: user()->username }} <br/>
                    Level Akun Anda : {{ Auth::user()->level }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
