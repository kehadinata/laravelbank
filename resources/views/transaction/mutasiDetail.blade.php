@extends('layouts.app')

@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Account</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          User Menu
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Mutasi
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            @if (Session::has('message'))
            <div class="alert {{ Session::get('alert') }}">
              <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
              </button>

              <strong>
                <i class="ace-icon fa fa-times"></i>
                Message
              </strong>

              {{ Session::get('message')}}
              <br />
            </div>
            @endif
            <div class="col-xs-12">
              <table id="simple-table" class="table  table-bordered table-hover">
                <thead>
                  <tr>

                    <th>Transaction ID</th>
                    <th>Account Number</th>
                    <th>Transaction Detail</th>
                    <th>Date and Time</th>
                  </tr>
                </thead>

                <tbody>
                    @foreach($account[0]['transaction'] as $accounts)
                    <tr>
                      <td>
                        {{ $accounts['tr_id'] }}
                      </td>
                      <td>
                        {{ $accounts['account_number'] }}
                      </td>
                      <td>
                        {{ $accounts['transaction_detail'] }}
                      </td>
                      <td>
                        {{ date('d F Y h:m',strtotime($accounts['created_at'])) }}
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

@endsection
