@extends('layouts.app')

@section('content')
<div class="main-content">
  <?php if(Session::has('after_save')){ ?>
  <div class="alert {{ Session::get('after_save.alert') }}">
    <button type="button" class="close" data-dismiss="alert">
      <i class="ace-icon fa fa-times"></i>
    </button>

    <strong>
      <i class="ace-icon fa {{ Session::get('after_save.icon') }}"></i>
      Message
    </strong>

    {{ Session::get('after_save.message')}}
    <br />
  </div>
  <?php } ?>

  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Transaction</li>
      </ul><!-- /.breadcrumb -->

    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          Transaction
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            All Balance
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            <div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-1" style="margin-bottom:20px;">
              <div class="widget-box" id="widget-box-1">
                <div class="widget-header">
                  <h5 class="widget-title">Total Money Information</h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <p class="alert alert-info">
                      Total Money in This Bank : Rp. {!! number_format($money) !!}
                    </p>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-xs-12">


            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

<script type="text/text/javascript">
  function checkForm(form)
  {
    if(Number(form.curbalance.value) > Number(form.deposit.value)) {

    } else {
      alert("Error: Your transfer amount is bigger than your current balance!");
      form.deposit.focus();
      return false;
    }

    return true;
  }
</script>

@endsection
