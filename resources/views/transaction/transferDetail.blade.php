@extends('layouts.app')

@section('content')
<div class="main-content">
  <?php if(Session::has('after_save')){ ?>
  <div class="alert {{ Session::get('after_save.alert') }}">
    <button type="button" class="close" data-dismiss="alert">
      <i class="ace-icon fa fa-times"></i>
    </button>

    <strong>
      <i class="ace-icon fa {{ Session::get('after_save.icon') }}"></i>
      Message
    </strong>

    {{ Session::get('after_save.message')}}
    <br />
  </div>
  <?php } ?>

  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Transaction</li>
      </ul><!-- /.breadcrumb -->

    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          Transaction
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Transfer
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            <div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-1" style="margin-bottom:20px;">
              <div class="widget-box" id="widget-box-1">
                <div class="widget-header">
                  <h5 class="widget-title">Account Destination Information</h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <p class="alert alert-info">
                      Name : {{ $destination[0]['user']['name'] }}
                    </p>
                    <p class="alert alert-success">
                      Account Number : {{$destination[0]['account_number'] }}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <form method="POST" action="{{ URL('account/transferStore') }}" aria-label="{{ __('Trabsfer Store') }}" onsubmit="return checkForm(this);">
                @csrf


            <div class="col-xs-12">

              <input type="hidden" name="source" value="{{$account->id_account}}" />
              <input type="hidden" name="dest" value="{{ $destination[0]['id_account'] }}" />
              <input type="hidden" id="curbalance" name="curbalance" value="{{$account->balance}}" />
              <div class="form-group row">
                  <label for="PIN" class="col-md-4 col-form-label text-md-right">{{ __('Security PIN') }}</label>

                  <div class="col-md-6">
                      <input id="pin" type="password" class="form-control{{ $errors->has('pin') ? ' is-invalid' : '' }}" name="pin" value="{{ old('pin') }}" maxlength="6" required autofocus>

                      @if ($errors->has('pin'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('pin') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="deposit" class="col-md-4 col-form-label text-md-right">{{ __('Transfer Value (Max Rp. 10.000.000)') }}</label>

                  <div class="col-md-6">
                      <input id="deposit" type="number" class="form-control{{ $errors->has('deposit') ? ' is-invalid' : '' }}" name="deposit" value="{{ old('deposit') }}" max="10000000" required autofocus>

                      @if ($errors->has('deposit'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('deposit') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>


              <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure to transfer?')">
                          {{ __('Transfer Balance') }}
                      </button>
                  </div>
              </div>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

<script type="text/text/javascript">
  function checkForm(form)
  {
    if(Number(form.curbalance.value) > Number(form.deposit.value)) {

    } else {
      alert("Error: Your transfer amount is bigger than your current balance!");
      form.deposit.focus();
      return false;
    }

    return true;
  }
</script>

@endsection
