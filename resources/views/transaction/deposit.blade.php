@extends('layouts.app')

@section('content')
<div class="main-content">
  <?php if(Session::has('after_save')){ ?>
  <div class="alert {{ Session::get('after_save.alert') }}">
    <button type="button" class="close" data-dismiss="alert">
      <i class="ace-icon fa fa-times"></i>
    </button>

    <strong>
      <i class="ace-icon fa {{ Session::get('after_save.icon') }}"></i>
      Message
    </strong>

    {{ Session::get('after_save.message')}}
    <br />
  </div>
  <?php } ?>

  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Transaction</li>
      </ul><!-- /.breadcrumb -->

    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          Transaction
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Deposit
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            <form method="POST" action="{{ URL('account/depositStore') }}" aria-label="{{ __('Add Account') }}">
                @csrf


            <div class="col-xs-12">

              <div class="form-group row">
                <label for="account" class="col-md-4 col-form-label text-md-right">{{ __('Choose Account') }}</label>

                  <div class="col-md-6">
                      <select id="account" name="account" class="form-controll{{ $errors->has('account') ? ' is-invalid' : '' }}" required autofocus>
                        @foreach($accounts as $account)
                        @if($account->isdeactive == '0')
                          <option value="{{$account->id_account}}">{{$account->account_number}}</option>
                        @endif
                        @endforeach
                      </select>
                      @if ($errors->has('account'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('account') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="PIN" class="col-md-4 col-form-label text-md-right">{{ __('PIN (Must SIX Number)') }}</label>

                  <div class="col-md-6">
                      <input id="pin" type="password" class="form-control{{ $errors->has('pin') ? ' is-invalid' : '' }}" name="pin" value="{{ old('pin') }}" maxlength="6" required autofocus>

                      @if ($errors->has('pin'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('pin') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="deposit" class="col-md-4 col-form-label text-md-right">{{ __('Deposit Value (Max Rp. 5.000.000)') }}</label>

                  <div class="col-md-6">
                      <input id="deposit" type="number" class="form-control{{ $errors->has('deposit') ? ' is-invalid' : '' }}" name="deposit" value="{{ old('deposit') }}" max="5000000" required autofocus>

                      @if ($errors->has('deposit'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('deposit') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>


              <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you want to do this deposit?')">
                          {{ __('Deposit Money') }}
                      </button>
                  </div>
              </div>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

@endsection
