@extends('layouts.app')

@section('content')
<div class="main-content">
  <?php if(Session::has('after_save')){ ?>
  <div class="alert {{ Session::get('after_save.alert') }}">
    <button type="button" class="close" data-dismiss="alert">
      <i class="ace-icon fa fa-times"></i>
    </button>

    <strong>
      <i class="ace-icon fa {{ Session::get('after_save.icon') }}"></i>
      Message
    </strong>

    {{ Session::get('after_save.message')}}
    <br />
  </div>
  <?php } ?>
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Account</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          User Menu
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Account
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            @if (Session::has('message'))
            <div class="alert {{ Session::get('alert') }}">
              <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
              </button>

              <strong>
                <i class="ace-icon fa fa-times"></i>
                Message
              </strong>

              {{ Session::get('message')}}
              <br />
            </div>
            @endif
            <a href="{{ URL('account/create') }}" class="btn btn-raised btn-primary pull-left" style="margin-bottom:10px;margin-left:10px;">Tambah</a>
            <div class="col-xs-12">
              <table id="simple-table" class="table  table-bordered table-hover">
                <thead>
                  <tr>

                    <th>Account ID</th>
                    <th>Balance</th>
                    <th>Created Date</th>
                    <th class="hidden-480">Status</th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>
                    @foreach($accounts as $account)
                    <tr>
                      <td>
                        {{ $account->account_number }}
                      </td>
                      <td>
                        Rp. {{ number_format($account->balance) }}
                      </td>
                      <td>
                        {{ $account->created_at }}
                      </td>

                      <td class="hidden-480">
                        @if($account->isdeactive == '0')
                          <span class="label label-sm label-success">Active</span>
                        @else
                          <span class="label label-sm label-danger">Inactive</span>
                        @endif
                      </td>

                      <td>
                        <div class="btn-group">
                          <a href="{{ url('account/deactive') }}/{{$account->id_account}}" class="btn btn-xs btn-danger" data-rel="tooltip" title="Deactive" onclick="return confirm('Are you sure want to deactive this account?')">
                            <i class="ace-icon fa fa-close bigger-120" title="Deactive"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

@endsection
