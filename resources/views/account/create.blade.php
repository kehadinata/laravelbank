@extends('layouts.app')

@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Account</li>
      </ul><!-- /.breadcrumb -->

    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          User Menu
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Create Account
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            <form method="POST" action="{{ URL('account/store') }}" aria-label="{{ __('Add Account') }}">
                @csrf


            <div class="col-xs-12">

              <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">{{ __('New Account Number') }}</label>
                <label for="PIN" class="col-md-4 col-form-label text-md-right">{{ $account_number }}</label>

                  <div class="col-md-6">
                      <input id="account_number" type="hidden" class="form-control{{ $errors->has('account_number') ? ' is-invalid' : '' }}" name="account_number" value="{{ $account_number }}" required autofocus>

                      @if ($errors->has('account_number'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('account_number') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="PIN" class="col-md-4 col-form-label text-md-right">{{ __('PIN (Must SIX Number)') }}</label>

                  <div class="col-md-6">
                      <input id="pin" type="password" class="form-control{{ $errors->has('pin') ? ' is-invalid' : '' }}" name="pin" value="{{ old('pin') }}" maxlength="6" required autofocus>

                      @if ($errors->has('pin'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('pin') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="CPIN" class="col-md-4 col-form-label text-md-right">{{ __('Confirm PIN') }}</label>

                  <div class="col-md-6">
                      <input id="cpin" type="password" class="form-control{{ $errors->has('cpin') ? ' is-invalid' : '' }}" name="cpin" value="{{ old('cpin') }}" maxlength="6" required autofocus>

                      @if ($errors->has('cpin'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('cpin') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group row">
                  <label for="deposit" class="col-md-4 col-form-label text-md-right">{{ __('First Deposite (Min : Rp 500.000)') }}</label>

                  <div class="col-md-6">
                      <input id="deposit" type="number" class="form-control{{ $errors->has('deposit') ? ' is-invalid' : '' }}" name="deposit" value="{{ old('deposit') }}" min="500000" required autofocus>

                      @if ($errors->has('deposit'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('deposit') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>


              <div class="form-group row mb-0">
                  <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">
                          {{ __('Add New Account') }}
                      </button>
                  </div>
              </div>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

@endsection
