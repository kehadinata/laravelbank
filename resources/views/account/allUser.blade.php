@extends('layouts.app')

@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>

        <li>
          <a href="#">User Menu</a>
        </li>
        <li class="active">Account</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">

      <div class="page-header">
        <h1>
          User Menu
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            All User & Account
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
          <!-- PAGE CONTENT BEGINS -->
          <div class="row">
            @if (Session::has('message'))
            <div class="alert {{ Session::get('alert') }}">
              <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
              </button>

              <strong>
                <i class="ace-icon fa fa-times"></i>
                Message
              </strong>

              {{ Session::get('message')}}
              <br />
            </div>
            @endif
            <div class="col-xs-12">
              <table id="simple-table" class="table  table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="detail-col">Details</th>
                    <th>User Full Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                    <th>Created Date</th>
                  </tr>
                </thead>

                <tbody>
                    @foreach($accounts as $account)
                    <tr>
                      <td class="center">
                        <div class="action-buttons">
                          <a href="#" class="green bigger-140 show-details-btn" title="Show Details">
                            <i class="ace-icon fa fa-angle-double-down"></i>
                            <span class="sr-only">Details</span>
                          </a>
                        </div>
                      </td>
                      <td>
                        {{ $account['name'] }}
                      </td>
                      <td>
                        {{ $account['phone'] }}
                      </td>
                      <td>
                        {{ $account['address'] }}
                      </td>
                      <td>
                        {{ date('d F Y H:m',strtotime($account['created_at'])) }}
                      </td>

                    </tr>
                    <tr class="detail-row">
                      <td colspan="8">
                        <div class="table-detail">
                          <div class="row">
                            <div class="col-xs-12 col-sm-7">
                              <div class="space visible-xs"></div>
                              <h3>User Accounts</h3>
                              <div class="profile-user-info profile-user-info-striped">
                                @foreach($account['accounts'] as $userAccounts)
                                <div class="profile-info-row">
                                  <div class="profile-info-name" style="width:20%;"> Account Number </div>

                                  <div class="profile-info-value">
                                    <span>{{ $userAccounts['account_number'] }}</span>
                                  </div>
                                </div>

                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Balance </div>

                                  <div class="profile-info-value">
                                    <span>Rp. {{ number_format($userAccounts['balance']) }}</span>
                                  </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Created </div>

                                  <div class="profile-info-value">
                                    <span>{{ date('d F Y',strtotime($userAccounts['created_at'])) }}</span>
                                  </div>
                                </div>
                                <br/>
                                @endforeach

                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div><!-- /.span -->
          </div><!-- /.row -->


          <div class="hr hr-18 dotted hr-double"></div>



          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->

<script>
  $('.show-details-btn').on('click', function(e) {
    e.preventDefault();
    $(this).closest('tr').next().toggleClass('open');
    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
  });
</script>

@endsection
