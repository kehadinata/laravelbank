@extends('layouts.app')

@section('content')
<div class="main-container">
  <div class="main-content">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="login-container">
          <div class="center">
            <h3>
              <span class="green">Kevin Hadinata</span>
              <br/>
              <span class="grey" id="id-text2">Kevin Bank System</span>
            </h1>
            <h4 class="blue" id="id-company-text">&copy; Kevin Bank Corporation</h4>
          </div>

          <div class="space-6"></div>

          <div class="position-relative">
            <div id="login-box" class="login-box visible widget-box no-border">
              <div class="widget-body">
                <div class="widget-main">
                  <?php if(isset($error)){ ?>
                  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                      <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                      <i class="ace-icon fa fa-times"></i>
                      Uh oh! <br/>
                    </strong>

                    <?php echo $error; ?>
                    <br />
                  </div>
                <?php } ?>
                  <h4 class="header blue lighter bigger">
                    <i class="ace-icon fa fa-pencil green"></i>
                    PLEASE LOGIN TO CONTINUE
                  </h4>

                  <div class="space-6"></div>

                  <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                      @csrf
                    <fieldset>
                      <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                          <input id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                          <i class="ace-icon fa fa-user"></i>
                          @if ($errors->has('username'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('username') }}</strong>
                              </span>
                          @endif
                        </span>
                      </label>

                      <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                          <i class="ace-icon fa fa-lock"></i>

                          @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                        </span>
                      </label>

                      <div class="form-group row">
                          <div class="col-md-6 offset-md-4">
                              <div class="form-check">
                                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                  <label class="form-check-label" for="remember">
                                      {{ __('Remember Me') }}
                                  </label>
                              </div>
                          </div>
                      </div>

                      <div class="form-group row mb-0">
                          <div class="col-md-8 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  {{ __('Login') }}
                              </button>

                                  <a href="{{ route('register') }}" class="btn btn-success">
                                        {{ __('Register') }}
                                  </a>

                              <a class="btn btn-link" href="{{ route('password.request') }}">
                                  {{ __('Forgot Your Password?') }}
                              </a>
                          </div>
                      </div>

                      <div class="space"></div>

                      <div class="clearfix">
                        <!-- <label class="inline">
                          <input type="checkbox" class="ace" />
                          <span class="lbl"> Remember Me</span>
                        </label> -->


                      </div>

                      <div class="space-4"></div>
                    </fieldset>
                  </form>
                </div><!-- /.widget-main -->


              </div><!-- /.widget-body -->
            </div><!-- /.login-box -->
          </div><!-- /.position-relative -->
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.main-content -->
</div><!-- /.main-container -->
@endsection
