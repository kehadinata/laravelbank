<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'account'], function(){

    Route::get('/', 'AccountController@index');
    Route::get('/create', 'AccountController@create');
    Route::post('/store', 'AccountController@store');
    Route::get('/show/{id}', 'AccountController@show');
    Route::post('/update/{id}', 'AccountController@update');
    Route::get('/deactive/{id}', 'AccountController@deactive');
    Route::get('/deposit','AccountController@deposit');
    Route::post('/depositStore', 'AccountController@depositStore');
    Route::get('/transfer','AccountController@transfer');
    Route::post('/transferDetail','AccountController@transferDetail');
    Route::post('/transferStore','AccountController@transferStore');
    Route::get('/mutasi','AccountController@mutasi');
    Route::post('/mutasiDetail','AccountController@mutasiDetail');
    Route::get('/moneyTotal', 'AccountController@moneyTotal');
    Route::get('/allUser','AccountController@allUser');
    Route::get('/withdraw','AccountController@withdraw');
    Route::post('/withdrawStore', 'AccountController@withdrawStore');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
