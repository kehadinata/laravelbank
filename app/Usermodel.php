<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;
use App\Transaction;

class Usermodel extends Model
{
  protected $table = 'users';
  protected $primaryKey = 'id';

  function accounts() {
      return $this->hasMany('App\Account', 'id_user','id');
  }

  function transactions(){
      return $this->hasMany('App\Transaction','id_user','id');
  }

}
