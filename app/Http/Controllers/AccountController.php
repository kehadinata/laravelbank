<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Usermodel;
use App\Transaction;
use Auth;
use View;
use Session;

class AccountController extends Controller
{
  public function index(){
    $id = Auth::user()->id;
    $accounts = Usermodel::with('accounts')->find($id)->accounts;

    return View::make('account.view')->with('accounts',$accounts);
  }

  public function create(){

      $account_number = '52'.substr(strtotime(date('Y-m-d H:i:s')),2,8);

      return view('account.create',compact('account_number'));

  }

  public function deactive($id){
    $data = [
      'isdeactive' => '1',
      'updated_at' => date('Y-m-d h:i:s')
    ];
    $accounts = Account::where('id_account',$id)->update($data);
    $account = Account::find($id);

    $tr_id = 'Tr-Dec'.substr($account->account_number,8,10).'-'.strtotime(date('Y-m-d H:i:s'));

    $trans = [
      'id_user' => $account->id_user,
      'tr_id' => $tr_id,
      'account_number' => $account->account_number,
      'transaction_name' => 'Account Deactivate',
      'amount' => '',
      'transaction_detail' => 'Account Deactivate '.$account->account_number,
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
    $transaction = Transaction::insert($trans);

    $after_save = [
        'alert' => 'alert-success',
        'icon' => 'fa-check',
        'message' => 'Account Deactivate'
    ];

    return redirect()->back()->with('after_save', $after_save);

  }

  public function store(Request $request){
    $validate = \Validator::make($request->all(), [
              'pin' => 'required',
              'cpin' => 'required',
              'deposit' => 'required',
          ],

          // $after_save adalah isi session ketika form kosong dan di kembalikan lagi ke form dengan membawa session di bawah ini (lihat form bagian part alert), dengan keterangan error dan alert warna merah di ambil dari 'alert' => 'danger', dst.

          $after_save = [
              'alert' => 'danger',
              'title' => 'Oh wait!',
              'text-1' => 'Ada kesalahan',
              'text-2' => 'Silakan coba lagi.'
          ]);

      // jika form kosong maka artinya fails() atau gagal di proses, maka di return redirect()->back()->with('after_save', $after_save) artinya page di kembalikan ke form dengan membawa session after_save yang sudah kita deklarasi di atas.

      if($validate->fails()){
          return redirect()->back()->with('after_save', $after_save);
      }

      // $after_save adalah isi session ketika data berhasil disimpan dan di kembalikan lagi ke form dengan membawa session di bawah ini (lihat form bagian part alert), dengan keterangan success dan alert warna merah di ganti menjadi warna hijau di ambil dari 'alert' => 'success', dst.

      $after_save = [
          'alert' => 'success',
          'title' => 'Good Job!',
          'text-1' => 'Tambah lagi',
          'text-2' => 'Atau kembali.'
      ];

      // jika form tidak kosong artinya validasi berhasil di lalui maka proses di bawah ini di jalankan, yaitu mengambil semua kiriman dari form
      // nama_kendaraan,jenis_kendaraan,buatan,user_id adalah nama kolom yang ada di table kendaraan
      // sedangkan $request->nama_kendaraan adalah isi dari kiriman form

      $id = Auth::user()->id;

      $data = [
          'id_user' => $id,
          'account_number' => $request->account_number,
          'account_pin' => $request->pin,
          'balance' => $request->deposit,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s'),
          'isdeactive' => '0'
      ];

      // di bawah ini proses insert ke tabel kendaraan

      $store = Account::insert($data);

      // jika berhasil kembalikan ke page form dengan membawa session after_save success.

      return redirect('account');
  }

  public function deposit(){
    $id = Auth::user()->id;
    $accounts = Usermodel::with('accounts')->find($id)->accounts;

    return View::make('transaction.deposit')->with('accounts',$accounts);
  }

  public function depositStore(Request $request){
    $account = Account::find($request->account);
    if($account->account_pin == $request->pin){
      $totalbalance = $account->balance + $request->deposit;
      $data = [
        'balance' => $totalbalance,
        'updated_at' => date('Y-m-d H:i:s')
      ];
      $update = Account::where('id_account',$request->account)->update($data);

      $tr_id = 'Tr-Dep'.substr($account->account_number,8,10).'-'.strtotime(date('Y-m-d H:i:s'));

      $trans = [
        'id_user' => $account->id_user,
        'tr_id' => $tr_id,
        'account_number' => $account->account_number,
        'transaction_name' => 'Deposit',
        'amount' => $request->deposit,
        'transaction_detail' => 'Deposit balance Rp.'.$request->deposit,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ];
      $transaction = Transaction::insert($trans);

      $after_save = [
          'alert' => 'alert-success',
          'icon' => 'fa-check',
          'message' => 'Deposit Success balance now Rp.'.$totalbalance
      ];

      return redirect()->back()->with('after_save',$after_save);
    }else{

      $after_save = [
        'alert' => 'alert-danger',
        'icon' => 'fa-times',
        'message' => 'Pin yang anda masukkan tidak sesuai dengan akun yang anda pilih.'
      ];
      return redirect()->back()->with('after_save', $after_save);

    }

  }

  public function transfer(){
    $id = Auth::user()->id;
    $accounts = Usermodel::with('accounts')->find($id)->accounts;

    return View::make('transaction.transfer')->with('accounts',$accounts);
  }

  public function transferDetail(Request $request){
    $account = Account::find($request->account);
    $destination = Account::with('user')->where('account_number',$request->destination)->get()->toArray();
    if(empty($destination)){
      $after_save = [
        'alert' => 'alert-danger',
        'icon' => 'fa-times',
        'message' => 'Destinasi Akun tidak terdaftar !.'
      ];
      return redirect()->back()->with('after_save', $after_save);
    }else{
      return View::make('transaction.transferDetail')->with('account',$account)->with('destination',$destination);
    }
  }

  public function transferStore(Request $request){
    $destination = Account::find($request->dest);
    $account = Account::find($request->source);
    if($account->account_pin == $request->pin){
      $totalbalance = $destination->balance + $request->deposit;
      $data = [
        'balance' => $totalbalance,
        'updated_at' => date('Y-m-d H:i:s')
      ];
      $update = Account::where('id_account',$request->dest)->update($data);

      $curbalance = $account->balance - $request->deposit;
      $data2 = [
        'balance' => $curbalance,
        'updated_at' => date('Y-m-d H:i:s')
      ];
      $update = Account::where('id_account',$request->source)->update($data2);

      $tr_id = 'Tr-Tfb'.substr($account->account_number,8,10).'-'.strtotime(date('Y-m-d H:i:s'));

      $trans = [
        'id_user' => $account->id_user,
        'tr_id' => $tr_id,
        'account_number' => $account->account_number,
        'transaction_name' => 'Transfer Balance',
        'amount' => $request->deposit,
        'transaction_detail' => 'Transfer balance Rp.'.$request->deposit.' to '.$destination->account_number,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ];
      $transaction = Transaction::insert($trans);

      $after_save = [
          'alert' => 'alert-success',
          'icon' => 'fa-check',
          'message' => 'Transfer Success to '.$destination->account_number .', value : '. $request->deposit
      ];
      Session::flash('message','Transfer Success to '.$destination->account_number .', value : '. $request->deposit);
      Session::flash('alert','alert-success');

      return redirect('account');
    }else{

      $after_save = [
        'alert' => 'alert-danger',
        'icon' => 'fa-times',
        'message' => 'Pin yang anda masukkan tidak sesuai dengan akun yang anda pilih.'
      ];
      Session::flash('message','Pin yang anda masukkan tidak sesuai dengan akun yang anda pilih.');
      Session::flash('alert','alert-danger');

      return redirect('account');

    }

  }

  public function mutasi(){
    $id = Auth::user()->id;
    $accounts = Usermodel::with('accounts')->find($id)->accounts;

    return View::make('transaction.mutasi')->with('accounts',$accounts);
  }

  public function mutasiDetail(Request $request){
    $account = Account::with('transaction')->where('account_number',$request->account)->get()->toArray();
    if(empty($account)){
      $after_save = [
        'alert' => 'alert-danger',
        'icon' => 'fa-times',
        'message' => 'Akun tidak memiliki histori transaksi !.'
      ];
      return redirect()->back()->with('after_save', $after_save);
    }else{
      return View::make('transaction.mutasiDetail')->with('account',$account);
    }
  }

  public function moneyTotal(){
    $money = Account::all()->sum('balance');
    return View::make('transaction.balance')->with('money',$money);
  }

  public function allUser(){

    $accounts = Usermodel::with('accounts')->get()->toArray();
    return View::make('account.allUser')->with('accounts',$accounts);

  }

  public function withdraw(){
    $id = Auth::user()->id;
    $accounts = Usermodel::with('accounts')->find($id)->accounts;

    return View::make('transaction.withdraw')->with('accounts',$accounts);
  }

  public function withdrawStore(Request $request){
    $account = Account::find($request->account);
    if($account->account_pin == $request->pin){
      if($account->balance > $request->withdraw ){
        $totalbalance = $account->balance - $request->withdraw;
        $data = [
          'balance' => $totalbalance,
          'updated_at' => date('Y-m-d H:i:s')
        ];
        $update = Account::where('id_account',$request->account)->update($data);

        $tr_id = 'Tr-Wit'.substr($account->account_number,8,10).'-'.strtotime(date('Y-m-d H:i:s'));

        $trans = [
          'id_user' => $account->id_user,
          'tr_id' => $tr_id,
          'account_number' => $account->account_number,
          'transaction_name' => 'Withdraw',
          'amount' => $request->withdraw,
          'transaction_detail' => 'Withdraw balance Rp.'.$request->withdraw,
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ];
        $transaction = Transaction::insert($trans);

        $after_save = [
            'alert' => 'alert-success',
            'icon' => 'fa-check',
            'message' => 'Withdraw Success balance now Rp.'.$totalbalance
        ];

        return redirect()->back()->with('after_save',$after_save);
      }else{
        $after_save = [
          'alert' => 'alert-danger',
          'icon' => 'fa-times',
          'message' => 'Withdraw Failure , Please input lower withdraw amount.'
        ];
        return redirect()->back()->with('after_save', $after_save);
      }
    }else{

      $after_save = [
        'alert' => 'alert-danger',
        'icon' => 'fa-times',
        'message' => 'Pin yang anda masukkan tidak sesuai dengan akun yang anda pilih.'
      ];
      return redirect()->back()->with('after_save', $after_save);

    }

  }



}
