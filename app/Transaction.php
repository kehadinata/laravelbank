<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Usermodel;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $primaryKey = 'id_transaction';

    function user() {
        return $this->belongsTo('App\Usermodel','id_user','id');
    }

}
