<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Usermodel;
use App\Transaction;

class Account extends Model
{
    protected $table = 'accounts';
    protected $primaryKey = 'id_account';

    function user() {
        return $this->belongsTo('App\Usermodel','id_user','id');
    }

    function transaction() {
        return $this->hasMany('App\Transaction','account_number','account_number');
    }

}
