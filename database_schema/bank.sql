-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 09:20 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id_account` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_pin` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isdeactive` int(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id_account`, `id_user`, `account_number`, `account_pin`, `balance`, `created_at`, `updated_at`, `isdeactive`) VALUES
(3, 3, '5232680927', '123456', 1723456, '2018-07-27 01:42:07', '2018-07-30 00:20:24', 0),
(5, 3, '5232752295', '123456', 500000, '2018-07-27 21:31:45', '2018-07-27 21:32:32', 1),
(6, 10, '5232752613', '123456', 600000, '2018-07-27 21:37:02', '2018-07-27 21:38:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_26_161506_create_accounts_table', 2),
(4, '2018_07_27_205806_create_transactions_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id_transaction` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `tr_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_detail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id_transaction`, `id_user`, `tr_id`, `account_number`, `transaction_name`, `amount`, `transaction_detail`, `created_at`, `updated_at`) VALUES
(1, 3, 'Tr-Dep34', '5232682834', 'Deposit', '5000000', 'Deposit balance Rp.5000000', '2018-07-27 14:16:31', '2018-07-27 14:16:31'),
(2, 3, 'Tr-Tfb27-', '5232680927', 'Transfer Balance', '10000000', 'Transfer balance Rp.10000000 to 5232682834', '2018-07-27 14:25:07', '2018-07-27 14:25:07'),
(3, 3, 'Tr-Tfb34-1532726864', '5232682834', 'Transfer Balance', '2000000', 'Transfer balance Rp.2000000 to 5232680927', '2018-07-27 14:27:44', '2018-07-27 14:27:44'),
(4, 3, 'Tr-Dec95-1532752353', '5232752295', 'Account Deactivate', '', 'Account Deactivate 5232752295', '2018-07-27 21:32:33', '2018-07-27 21:32:33'),
(5, 10, 'Tr-Tfb13-1532752667', '5232752613', 'Transfer Balance', '100000', 'Transfer balance Rp.100000 to 5232680927', '2018-07-27 21:37:47', '2018-07-27 21:37:47'),
(6, 10, 'Tr-Dep13-1532752697', '5232752613', 'Deposit', '200000', 'Deposit balance Rp.200000', '2018-07-27 21:38:17', '2018-07-27 21:38:17'),
(7, 3, 'Tr-Wit27-1532935224', '5232680927', 'Withdraw', '1000000', 'Withdraw balance Rp.1000000', '2018-07-30 00:20:24', '2018-07-30 00:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `phone`, `address`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Kevin Hadinata Client', 'ckevin', 'kehadinata@yahoo.com', '$2y$10$zNdIe3jwBt5Nrlf6V3vmsefMbvemApsG/M.FKw2t/oIvhNWAQn4dq', '085770109091', 'Bekasi', 'client', 'mcreNRLqn1Il1qoy81vzuDalD3lJMhaY5yehXnHivDJbiYwbiHNsFIFrWRgK', '2018-07-25 22:35:27', '2018-07-25 22:35:27'),
(4, 'Kevin Hadinata Manager', 'mkevin', 'kehadinata93@yahoo.com', '$2y$10$Cp0mVDGLfAVyuJo4sMhzsOIPfGCWdcz48LngBsd.Jby/y5mZsWHYS', '085770109091', 'Bekasi', 'admin', '58iA1jyLMrxZA2f8VVcfYLiHHmxkAL0q77tGrlZKT8JMYYzFQOV8RiVYPt9K', '2018-07-25 22:36:48', '2018-07-25 22:36:48'),
(10, 'Kevin Hadinata Client2', 'ckevin2', 'kehadinata2@yahoo.com', '$2y$10$MzfjPwSbXXjhZC3DaksLXOA.xir1L.CQ001/WThLQ5QCi4LU2m6fC', '08119871993', 'Taman Harapan Baru', 'client', 'GZfptf7BhArOumH0DGOorSDCGJFCyaE83JlTjs2cAry4ynlOovzCWtktzg3B', '2018-07-27 21:36:48', '2018-07-27 21:36:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id_account`),
  ADD KEY `accounts_id_user_foreign` (`id_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id_transaction`),
  ADD KEY `transactions_id_user_foreign` (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id_account` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id_transaction` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
